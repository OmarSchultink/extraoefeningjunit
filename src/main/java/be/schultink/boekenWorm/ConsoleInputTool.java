package be.schultink.boekenWorm;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleInputTool {

    private static Scanner keyboard = new Scanner(System.in);


    public static int askUserInputInteger(String question, int minimum, int maximum) {
        int input = 0;
        do {
            input = askUserInputInteger(question);
            if (input < minimum) {
                System.err.println("Error: input moet gelijk of groter zijn dan " + minimum);
            } else if (input > maximum) {
                System.err.println("Error: input moet gelijk of kleiner zijn dan " + maximum);
            }
        } while (input < minimum || input > maximum);
        return input;
    }
    public static String askUserInputString(){
        String input = keyboard.nextLine();
        return input;
    }
    public static int askUserInputInteger(String question) {
        int input = 0;
        try {
            System.out.print(question);
            input = keyboard.nextInt();
        } catch (InputMismatchException ime) {
            System.err.println("Error: input is not a number");
        } finally {
            keyboard.nextLine();
        }
        return input;
    }
}
