package be.schultink.boekenWorm;


import be.schultink.arrayManager.StringArrayManager;

import java.util.Scanner;

public class BoekenWormApp {

    private static StringArrayManager stringArrayManager = new StringArrayManager();


    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        BoekenWormApp.hoofdMenu();



    }

    static void hoofdMenu() {
        System.out.println("BoekenWorm Menu");
        System.out.println("1. ingeven boeken\n" +
                "2. kiezen boeken\n" +
                "3. overzicht boeken\n" +
                "0. stoppen");
        int keuze = ConsoleInputTool.askUserInputInteger("Maak een keuze van 0 t.e.m 3", 0, 3);

        switch (keuze) {

            case 0:
                break;
            case 1:
                registreerBoek();
                break;
            case 2:
                kiesBoek();
            case 3:
                overzichtBoeken();


        }
    }

    static void registreerBoek() {
        System.out.println("Geef stop in om terug te gaan naar het Menu");
        System.out.println("Geef titel");
        ConsoleInputTool.askUserInputString();
        if (ConsoleInputTool.askUserInputString() != "Stop") {
            System.out.println("Boek geregistreerd");
        } else if (ConsoleInputTool.askUserInputString() == "Stop") {
            hoofdMenu();
        }
        registreerBoek();

    }

    static void kiesBoek() {
        System.out.println("Boek keuze Menu");
        System.out.println("1. Eerste uit de lijst\n" +
                "2. Laatste uit de lijst\n" +
                "3. Willekeurig boek\n" +
                "4. Eerste alfabetisch boek\n" +
                "5. Laatste alfabetisch boek\n" +
                "0. Terug naar hoofdmenu ");
        int keuze = ConsoleInputTool.askUserInputInteger("Maak een keuze van 0 t.e.m 5", 0, 5);
        switch (keuze) {
            case 0:
                hoofdMenu();
            case 1:
                stringArrayManager.getFirstValue();
            case 2:
                stringArrayManager.getLastValue();
            case 3:
                stringArrayManager.getRandomValue();
            case 4:
                stringArrayManager.getLowestValue();
            case 5:
                stringArrayManager.getHighestValue();

        }
    }

    static void overzichtBoeken() {

    }



}
